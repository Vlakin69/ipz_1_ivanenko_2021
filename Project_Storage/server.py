import socket               # Import socket module``
from lib import *
import pickle
import pymysql
import time

def connect_to_bd():
    try:
        connection = pymysql.connect(  # Создаем соединение с нашей базой данных
            host='35.226.211.14',
            port=3306,
            user='vlados',
            password='vlados',
            database='ipzproject',
            cursorclass=pymysql.cursors.DictCursor
        )
        print("Successfully connected to bd")
        return connection

    except Exception as ex:
        print("Connection refused")

def LogIn(connection, email, password):
    with connection.cursor() as cursor:
        insert_query = ("SELECT * FROM clients WHERE (email = %s AND client_password=%s )")
        cursor.execute(insert_query, (email, password))
        rows = cursor.fetchall()
        if not rows:
            return False
        elif rows[0].get('status') == 'admin':
            return True, 'admin'
        else:
            return True, email, 'user', rows[0].get('client_name'), rows[0].get('client_surname')

def Registration(connection, name, surname,  email, password, status):
    with connection.cursor() as cursor:
        insert_query = ("INSERT INTO clients (client_name , client_surname, email, client_password, status ) VALUES (%s,%s,%s,%s,%s)")
        cursor.execute(insert_query, ( name, surname, email, password, status))
        connection.commit()
        return True

def CheckEmail(connection, email):
    with connection.cursor() as cursor:
        insert_query = ("SELECT email FROM clients WHERE email = %s")
        cursor.execute(insert_query, email)
        rows = cursor.fetchall()
        if not rows:
            return True
        else:
            return False

def Sections(connection):
        N = OrdersCount(connection)
        if 0<N<11:
            return 'A'
        elif 10<=N<21:
            return 'B'
        elif 20<=N<31:
            return 'C'


def Scheme(connection):
    with connection.cursor() as cursor:
        insert_query = ("SELECT `sections`, COUNT(*) FROM `storage` GROUP BY `sections`")
        cursor.execute(insert_query)
        rows = cursor.fetchall()
        return rows

def OrdersCount(connection):
    with connection.cursor() as cursor:
        insert_query = ("SELECT * FROM `storage`")
        cursor.execute(insert_query)
        rows = cursor.fetchall()
        N = len(rows)+1
        return N


def PutInStorage (connection, name, quantity, weight, size, date, phone_number, email, sections):
    if OrdersCount(connection) < 30:
        with connection.cursor() as cursor:
            insert_query = ("INSERT INTO storage (order_name , order_quantity, order_weight, order_size, order_date, order_phone_number, email, sections) VALUES (%s,%s,%s,%s,%s,%s,%s, %s) ")
            cursor.execute(insert_query, (name, quantity, weight, size, date, phone_number, email, sections))
            connection.commit()
            return True
    return False

def TakeFromStorage (connection, id_order, address, date, phone_number, email):
    orders = ShowAllOrders(connection)
    for i in orders:
        if str(i.get('order_id')) == str(id_order):
            with connection.cursor() as cursor:
                insert_query = ("INSERT INTO from_storage (take_order_id, address, take_order_date, phone_number, email) VALUES (%s,%s,%s,%s,%s) ")
                cursor.execute(insert_query, (id_order, address, date, phone_number, email))
                connection.commit()
                return True
    return False

def ShowClientOrders(connection, email):
    with connection.cursor() as cursor:
        insert_query = ("SELECT storage.order_id, storage.order_name, storage.order_quantity, storage.order_weight, storage.order_size, storage.order_date, storage.order_phone_number, clients.email  FROM storage INNER JOIN clients ON storage.email = clients.email")
        cursor.execute(insert_query)
        rows = cursor.fetchall()
        orders = []
        for row in rows:
            if email in row.values():
                orders.append(row)
        return orders

def ShowClientTakeOrders(connection, email):
    with connection.cursor() as cursor:
        insert_query = ("SELECT from_storage.id, from_storage.take_order_id, from_storage.address, from_storage.take_order_date, from_storage.phone_number, from_storage.email  FROM from_storage INNER JOIN clients ON from_storage.email = clients.email")
        cursor.execute(insert_query)
        rows = cursor.fetchall()
        orders = []
        for row in rows:
            if email in row.values():
                orders.append(row)
        return orders

def DeleteOrder(connection,id_order, e_mail):
    orders = ShowClientOrders(connection, e_mail)
    for i in orders:
        if str(i.get('order_id')) == str(id_order):
            with connection.cursor() as cursor:
                insert_query = ("DELETE FROM storage WHERE (order_id = %s AND email= %s) ")
                cursor.execute(insert_query, (id_order, e_mail))
                connection.commit()
                return True

    return False

def ShowAllOrders(connection):
    with connection.cursor() as cursor:
        insert_query = ("SELECT *  FROM storage")
        cursor.execute(insert_query)
        rows = cursor.fetchall()
        return rows
def ShowAllClients(connection):
    with connection.cursor() as cursor:
        insert_query = ("SELECT *  FROM clients WHERE status = 'client'")
        cursor.execute(insert_query)
        rows = cursor.fetchall()
        return rows



port = 3389             # Reserve a port for your service.
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Create a socket object
s.bind(('', port))  # Bind to the port
s.listen()
print('socket is listening')
while True:
    c, addr = s.accept()  # Establish connection with client.
    print('CONNECTED')  # Now wait for client connection.
    try:
        bytes = c.recv(65000)
        command = pickle.loads(bytes)
        print(command.instraction)

        if command.instraction == 'LogIn':
            bytes = c.recv(50000)
            logIn = pickle.loads(bytes)
            accept = LogIn(connect_to_bd(), logIn.email, logIn.password)
            c.sendall(pickle.dumps(accept))


        elif command.instraction == 'Registration':
            bytes = c.recv(2048)
            user = pickle.loads(bytes)
            accept= Registration(connect_to_bd(), user.name, user.surname, user.email, user.password, user.status)
            c.sendall(pickle.dumps(accept))

        elif command.instraction == 'CheckEmail':
            bytes = c.recv(50000)
            check = pickle.loads(bytes)
            accept = CheckEmail(connect_to_bd(), check.email)
            c.sendall(pickle.dumps(accept))


        elif command.instraction == 'PutInStorage':
            bytes = c.recv(65000)
            put_in_storage = pickle.loads(bytes)
            accept = PutInStorage(connect_to_bd(), put_in_storage.name, put_in_storage.quantity, put_in_storage.weight, put_in_storage.size, put_in_storage.date,put_in_storage.phone_number, put_in_storage.email, Sections(connect_to_bd()))
            c.sendall(pickle.dumps(accept))


        elif command.instraction == 'TakeFromStorage':
            bytes = c.recv(65000)
            take_from_storage = pickle.loads(bytes)
            accept = TakeFromStorage(connect_to_bd(), take_from_storage.id_order, take_from_storage.address, take_from_storage.date,take_from_storage.phone_number, take_from_storage.email)
            c.sendall(pickle.dumps(accept))


        elif command.instraction == 'ShowClientOrders':
            bytes = c.recv(2048)
            emails = pickle.loads(bytes)
            orders = ShowClientOrders(connect_to_bd(), emails.email)
            c.sendall(pickle.dumps(orders))

        elif command.instraction == 'ShowClientTakeOrders':
            bytes = c.recv(2048)
            emails = pickle.loads(bytes)
            take_orders = ShowClientTakeOrders(connect_to_bd(), emails.email)
            c.sendall(pickle.dumps(take_orders))

        elif command.instraction == 'DeleteOrder':
            bytes = c.recv(2048)
            id_del_order = pickle.loads(bytes)
            del_order = DeleteOrder(connect_to_bd(), id_del_order.id_order,id_del_order.email)#,id_del_order.email
            c.sendall(pickle.dumps(del_order))

        elif command.instraction == 'ShowAllOrders':
            ShowOrders = ShowAllOrders(connect_to_bd())
            c.sendall(pickle.dumps(ShowOrders))

        elif command.instraction == 'ShowAllClients':
            ShowClients = ShowAllClients(connect_to_bd())
            c.sendall(pickle.dumps(ShowClients))

        elif command.instraction == 'SchemeOfSections':
            try:
                Scheme = Scheme(connect_to_bd())
            except TypeError:
                print('wrong smth')
            c.sendall(pickle.dumps(Scheme))

        elif command.instraction == 'Wait':
            print("Wait for data from user.")

        print('end command')
    except EOFError:
        print('Waiting for connection')
    except ConnectionResetError:
        print('Waiting for connection')
