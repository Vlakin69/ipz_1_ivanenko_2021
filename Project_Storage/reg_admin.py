from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import Qt,  QPoint
from PyQt5.QtWidgets import QMessageBox
from GUI.registration_admin_gui import Ui_RegAdmin
from lib import *
import socket
import pickle
import time
import sys
import validators

host = '104.154.19.99' # Get local machine name
port = 3389      # Reserve a port for your service.

class RegWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.registration_gui = Ui_RegAdmin()
        self.registration_gui.setupUi(self)
        self.registration_gui.reg_password_lineedit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.registration_gui.reg_conf_password_lineedit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.__press_pos = QPoint()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.init_UI_RegWindow()

    def registration(self):
        try:
            try:
                s = socket.socket()
                s.connect((host, port))
            except ConnectionRefusedError:
                back = QMessageBox()
                back.setWindowTitle('Помилка')
                back.setText("Немає з'єднання")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()
            name = self.registration_gui.reg_name_lineedit.text()
            surname = self.registration_gui.reg_surname_lineedit.text()
            email = self.registration_gui.reg_email_lineedit.text()
            password = self.registration_gui.reg_password_lineedit.text()
            password_conf = self.registration_gui.reg_conf_password_lineedit.text()
            check_status = self.registration_gui.label.text()
            valid_email = validators.email(email)
            valid_password = validators.length(password, min=8)

            if name.isalpha() == True and surname.isalpha() == True and valid_email == True and valid_password == True and password_conf == password:
                    status = 'admin'
                    user = ClientRegistration(name, surname, email, password, status)
                    check_email = CheckEmail(email)

                    command_chek = Commands('CheckEmail')
                    s.send(pickle.dumps(command_chek))
                    time.sleep(1)
                    s.send(pickle.dumps(check_email))
                    bytes = s.recv(65000)
                    accept = pickle.loads(bytes)
                    s.close()

                    if accept == True:
                        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        s.connect((host, port))
                        command = Commands('Registration')
                        s.send(pickle.dumps(command))
                        time.sleep(0.00001)
                        s.send(pickle.dumps(user))  # REGISTRSTION!!!!!!!!!!
                        bytes = s.recv(65000)
                        pop_up = pickle.loads(bytes)
                        s.close()
                        if pop_up == True:
                            back = QMessageBox()
                            back.setWindowTitle('Реєстрація!')
                            back.setText("Користувача зареєстровано!")
                            back.setIcon(QMessageBox.Information)
                            back.setStandardButtons(QMessageBox.Ok)
                            back.exec_()
                    else:
                        back = QMessageBox()
                        back.setWindowTitle('Помилка!')
                        back.setText("Користувач з таким емейлом уже існує.")
                        back.setIcon(QMessageBox.Warning)
                        back.setStandardButtons(QMessageBox.Ok)
                        back.exec_()


            elif valid_email != True:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Некоректний емейл.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()

            elif name.isalpha() == False:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Ім'я має містити тільки літери.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()

            elif surname.isalpha() == False:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Прізвище має містити тільки літери.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()

            elif password_conf != password:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Паролі не збігаються.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()


            else:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Пароль має містити щонайменше 8 симолів.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()
        except OSError:
                print('Invalid connection')

    def close_w(self):
        self.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = QPoint()

    def mouseMoveEvent(self, event):
        if not self.__press_pos.isNull():
            self.move(self.pos() + (event.pos() - self.__press_pos))

    def init_UI_RegWindow(self):
        self.registration_gui.reg_btn.clicked.connect(self.registration)
        self.registration_gui.close_btn.clicked.connect(self.close_w)

app = QtWidgets.QApplication([])
application = RegWindow()
application.show()
sys.exit(app.exec_())