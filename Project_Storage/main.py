from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtCore import Qt,  QPoint
from PyQt5.QtWidgets import QMessageBox
import sys
# from GUI.GUI import Ui_LoginWindow, Ui_RegistrationWindow, Ui_UserWindow, Ui_OrderWindow, Ui_TakeOrderWindow, Ui_DeleteOrderWindow, Ui_AdminWindow, Ui_ShowInfoWindow
from GUI.sign_in_gui import Ui_LoginWindow
from GUI.registration_gui import Ui_RegistrationWindow
from GUI.user_gui import Ui_UserWindow
from GUI.take_from_storage_gui import Ui_TakeOrderWindow
from GUI.delete_order_gui import Ui_DeleteOrderWindow
from GUI.adminisration_gui import Ui_AdminWindow
from GUI.show_screen_gui import Ui_ShowInfoWindow
from GUI.put_in_storage_gui import Ui_OrderWindow
from lib import *
import socket
import pickle
import time

import validators

host = '104.154.19.99' # Get local machine name
port = 3389        # Reserve a port for your service.

class LogWindow(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.sign_in_gui = Ui_LoginWindow()
        self.sign_in_gui.setupUi(self)
        self.sign_in_gui.user_password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.__press_pos = QPoint()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.init_UI_LogWindow()

    def login_btn_clicked(self):
        try:
            email = self.sign_in_gui.user_email.text()
            password = self.sign_in_gui.user_password.text()

            valid_email = validators.email(email)
            valid_password = validators.truthy(password)

            if valid_email == True and valid_password == True:
                try:
                    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Create a socket object
                    s.connect((host, port))
                    print('Connect succesful')
                except ConnectionRefusedError:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка')
                    back.setText("Немає з'єднання")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()
                command = Commands('LogIn')
                login = ClientLogIn(email, password)

                # login
                s.sendall(pickle.dumps(command))
                time.sleep(0.00001)
                s.sendall(pickle.dumps(login))
                bytes = s.recv(65000)
                accept = pickle.loads(bytes)
                s.close()

                if accept==False:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка!')
                    back.setText("Неправильний логін або пароль.")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()

                elif True and 'user' in accept:
                    self.close()
                    self.userwindow = UserWindow(email, accept[3], accept[4])
                    self.userwindow.show()
                elif True and 'admin' in accept:
                    self.close()
                    self.adminwindow = AdminWindow()
                    self.adminwindow.show()
            else:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Не заповнені необхідні поля.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()
        except:
                print('Invalid connection')


    def reg_screen(self):
        self.close()
        self.regwindow = RegWindow()
        self.regwindow.show()

    def close_w(self):
        self.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = QPoint()

    def mouseMoveEvent(self, event):
        if not self.__press_pos.isNull():
            self.move(self.pos() + (event.pos() - self.__press_pos))

    def init_UI_LogWindow(self):
        self.sign_in_gui.login_btn.clicked.connect(self.login_btn_clicked)
        self.sign_in_gui.to_reg_btn.clicked.connect(self.reg_screen)
        self.sign_in_gui.close_btn.clicked.connect(self.close_w)

#############################################################################vlados19.2002@gmail.com
class RegWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.registration_gui = Ui_RegistrationWindow()
        self.registration_gui.setupUi(self)
        self.registration_gui.reg_password_lineedit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.registration_gui.reg_conf_password_lineedit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.__press_pos = QPoint()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.init_UI_RegWindow()

    def registration(self):
        try:
            name = self.registration_gui.reg_name_lineedit.text()
            surname = self.registration_gui.reg_surname_lineedit.text()
            email = self.registration_gui.reg_email_lineedit.text()
            password = self.registration_gui.reg_password_lineedit.text()
            password_conf = self.registration_gui.reg_conf_password_lineedit.text()
            check_status = self.registration_gui.label.text()
            valid_email = validators.email(email)
            valid_password = validators.length(password, min=8)

            if name.isalpha() == True and surname.isalpha() == True and valid_email == True and valid_password == True and password_conf == password:
                try:
                    s = socket.socket()
                    s.connect((host, port))
                    print('Connect succesful')
                except ConnectionRefusedError:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка')
                    back.setText("Немає з'єднання")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()
                status = 'client'
                user = ClientRegistration(name, surname, email, password, status)
                check_email = CheckEmail(email)

                command_chek = Commands('CheckEmail')
                s.send(pickle.dumps(command_chek))
                time.sleep(1)
                s.send(pickle.dumps(check_email))
                bytes = s.recv(65000)
                accept = pickle.loads(bytes)
                s.close()

                if accept == True:
                    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    s.connect((host, port))
                    command = Commands('Registration')
                    s.send(pickle.dumps(command))
                    time.sleep(0.00001)
                    s.send(pickle.dumps(user))  # REGISTRSTION!!!!!!!!!!
                    bytes = s.recv(65000)
                    pop_up = pickle.loads(bytes)
                    s.close()
                    if pop_up == True:
                        back = QMessageBox()
                        back.setWindowTitle('Реєстрація!')
                        back.setText("Користувача зареєстровано!")
                        back.setIcon(QMessageBox.Information)
                        back.setStandardButtons(QMessageBox.Ok)
                        back.buttonClicked.connect(self.goback)
                        back.exec_()
                else:
                    back = QMessageBox()
                    back.setWindowTitle('Помилка!')
                    back.setText("Користувач з таким емейлом уже існує.")
                    back.setIcon(QMessageBox.Warning)
                    back.setStandardButtons(QMessageBox.Ok)
                    back.exec_()


            elif valid_email != True:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Некоректний емейл.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()

            elif name.isalpha() == False:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Ім'я має містити тільки літери.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()

            elif surname.isalpha() == False:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Прізвище має містити тільки літери.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()

            elif password_conf != password:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Паролі не збігаються.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()


            else:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("Пароль має містити щонайменше 8 симолів.")
                back.setIcon(QMessageBox.Warning)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()
        except OSError:
                print('Invalid connection')

    def goback(self):
        self.close()
        self.application = LogWindow()
        self.application.show()

    def close_w(self):
        self.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = QPoint()

    def mouseMoveEvent(self, event):
        if not self.__press_pos.isNull():
            self.move(self.pos() + (event.pos() - self.__press_pos))

    def init_UI_RegWindow(self):
        self.registration_gui.reg_btn.clicked.connect(self.registration)
        self.registration_gui.back_btn_2.clicked.connect(self.goback)
        self.registration_gui.close_btn.clicked.connect(self.close_w)
#############################################################################vlados19.2002@gmail.com
class UserWindow(QtWidgets.QMainWindow):
    email = ''
    name = ''
    surname = ''
    def __init__(self, e_mail, name, surname):
        super().__init__()
        UserWindow.email = e_mail
        UserWindow.name = name
        UserWindow.surname = surname
        self.user_gui = Ui_UserWindow()
        self.user_gui.setupUi(self)
        self.user_gui.edit_user_name.setText(UserWindow.name)
        self.user_gui.edit_user_surname.setText(UserWindow.surname)
        self.user_gui.edit_user_email.setText(UserWindow.email)
        self.__press_pos = QPoint()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.init_UI_UserWindow()



    def order_sreen(self):
        self.close()
        self.orderwindow = OrderWindow(UserWindow.email,UserWindow.name, UserWindow.surname)
        self.orderwindow.show()

    def show_client_orders(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        command = Commands('ShowClientOrders')
        email = Email(UserWindow.email)
        s.sendall(pickle.dumps(command))
        time.sleep(0.00001)
        s.sendall(pickle.dumps(email))
        bytes = s.recv(65000)
        orders = pickle.loads(bytes)
        s.close()
        user_order_list = []
        for i in orders:
            user_order_list.append("Номер замовлення: " + str(i.get('order_id')) +
                                   '\n'+"Найменування: " + str(i.get('order_name')) +
                                   '\n' + "Дата: " + str(i.get('order_date')) + '\n\n')
        #******************************************
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        command = Commands('ShowClientTakeOrders')
        email = Email(UserWindow.email)
        s.sendall(pickle.dumps(command))
        time.sleep(0.00001)
        s.sendall(pickle.dumps(email))
        bytes = s.recv(65000)
        take_orders = pickle.loads(bytes)
        s.close()
        user_take_order_list = []
        for i in take_orders:
            user_take_order_list.append("Номер замовлення: " + str(i.get('id')) + '\n' +
                                        "Номер замовлення (на складі): " + str(i.get('take_order_id')) +
                                        '\n' + "Дата: " + str(i.get('take_order_date')) + '\n' +"Адреса: " +
                                        str(i.get('address'))  + '\n\n')

        self.user_gui.all_order_textEdit.setFont(QtGui.QFont("Times", 12 ))
        self.user_gui.all_order_textEdit.setText('Заявки зберігання на складі\n')
        self.user_gui.all_order_textEdit.setAlignment(Qt.AlignCenter)
        self.user_gui.all_order_textEdit.append(str(''.join(user_order_list)))
        self.user_gui.all_order_textEdit.setAlignment(Qt.AlignCenter)
        self.user_gui.all_order_textEdit.append('Заявки видачі зі складу\n')
        self.user_gui.all_order_textEdit.setAlignment(Qt.AlignCenter)
        self.user_gui.all_order_textEdit.append(str(''.join(user_take_order_list)))
        self.user_gui.all_order_textEdit.setAlignment(Qt.AlignCenter)


    def delet_eorder_sreen(self):
        self.close()
        self.delet_eorder_sreen_window = DeleteOrderWindow(UserWindow.email, UserWindow.name, UserWindow.surname)
        self.delet_eorder_sreen_window.show()

    def take_order_sreen(self):
        self.close()
        self.takeorderwindow = TakeOrderWindow(UserWindow.email,UserWindow.name, UserWindow.surname)
        self.takeorderwindow.show()

    def goback(self):
        self.close()
        self.application = LogWindow()
        self.application.show()

    def close_w(self):
        self.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = QPoint()

    def mouseMoveEvent(self, event):
        if not self.__press_pos.isNull():
            self.move(self.pos() + (event.pos() - self.__press_pos))

    def init_UI_UserWindow(self):
        self.show_client_orders()
        self.user_gui.put_btn.clicked.connect(self.order_sreen)
        self.user_gui.back_btn_2.clicked.connect(self.goback)
        self.user_gui.delete_order_btn.clicked.connect(self.delet_eorder_sreen)
        self.user_gui.take_btn.clicked.connect(self.take_order_sreen)
        self.user_gui.close_btn.clicked.connect(self.close_w)
#############################################################################vlados19.2002@gmail.com
class OrderWindow(QtWidgets.QMainWindow):
    email = ''
    name = ''
    surname = ''
    def __init__(self, e_mail, name, surname):
        super().__init__()
        OrderWindow.email = e_mail
        OrderWindow.name = name
        OrderWindow.surname = surname
        self.put_in_storage_gui = Ui_OrderWindow()
        self.put_in_storage_gui.setupUi(self)
        self.put_in_storage_gui.order_date.setMinimumDate(QtCore.QDate.currentDate())
        self.__press_pos = QPoint()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.init_UI_OrderWindow()


    def write(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))


        name = self.put_in_storage_gui.order_name_lineEdit.text()
        quantity = self.put_in_storage_gui.order_quantity.text()
        weight = self.put_in_storage_gui.order_weight.text()
        size = self.put_in_storage_gui.order_size.text()
        date = self.put_in_storage_gui.order_date.text()
        phone_number = self.put_in_storage_gui.order_phone_number.text()

        valid_name = validators.truthy(name)
        valid_phone_number = validators.length(phone_number, min=10)

        if valid_name == True and int(quantity) > 0 and weight != '0,00' and size != '0,00' and  valid_phone_number == True and phone_number.isdigit() == True:
            command = Commands('PutInStorage')
            put_in_storage = ToStorage(name, quantity, weight, size, date, phone_number, OrderWindow.email)

            s.sendall(pickle.dumps(command))
            time.sleep(1)
            s.sendall(pickle.dumps(put_in_storage))
            bytes = s.recv(65000)
            accept = pickle.loads(bytes)
            s.close()
            if accept == True:
                back = QMessageBox()
                back.setWindowTitle('Заявка')
                back.setText("Дякуємо! Ваша заявка буде оброблена.\n З вами зв'яжеться адміністратор.")
                back.setIcon(QMessageBox.Information)
                back.setStandardButtons(QMessageBox.Ok)
                back.buttonClicked.connect(self.goback)
                back.exec_()
            elif accept == False:
                back = QMessageBox()
                back.setWindowTitle('Заявка')
                back.setText("Наразі склад заповнено.")
                back.setIcon(QMessageBox.Information)
                back.setStandardButtons(QMessageBox.Ok)
                back.buttonClicked.connect(self.goback)
                back.exec_()

        elif valid_name != True:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Заповніть поле з назвою!")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()

        elif int(quantity) <= 0:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Вкажіть кількість.")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()

        elif weight == '0,00':
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Вкажіть вагу.")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()

        elif size == '0,00':
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Вкажіть об'єм.")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()

        elif valid_phone_number != True:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Номер телефону має містити 10 цифр.")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()

        elif phone_number.isdigit() == False:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Номер телефону має містити лише цифри.")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()


        else:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Некоректно заповнені поля!")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()




    def goback(self):
        self.close()
        self.userwindow = UserWindow(OrderWindow.email, OrderWindow.name, OrderWindow.surname)
        self.userwindow.show()

    def close_w(self):
        self.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = QPoint()

    def mouseMoveEvent(self, event):
        if not self.__press_pos.isNull():
            self.move(self.pos() + (event.pos() - self.__press_pos))

    def init_UI_OrderWindow(self):
        self.put_in_storage_gui.give_order_btn.clicked.connect(self.write)
        self.put_in_storage_gui.back_btn_2.clicked.connect(self.goback)
        self.put_in_storage_gui.close_btn.clicked.connect(self.close_w)

#############################################################################vlados19.2002@gmail.com
class TakeOrderWindow(QtWidgets.QMainWindow):
    email = ''
    name = ''
    surname = ''
    def __init__(self, e_mail, name, surname):
        super().__init__()
        TakeOrderWindow.email = e_mail
        TakeOrderWindow.name = name
        TakeOrderWindow.surname = surname

        self.take_from_storage_gui = Ui_TakeOrderWindow()
        self.take_from_storage_gui.setupUi(self)
        self.take_from_storage_gui.order_date.setMinimumDate(QtCore.QDate.currentDate())
        self.__press_pos = QPoint()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.init_UI_TakeOrderWindow()


    def write(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))

        id_order = self.take_from_storage_gui.order_id_lineEdit.text()
        address = self.take_from_storage_gui.order_addres_lineEdit_2.text()
        date = self.take_from_storage_gui.order_date.text()
        phone_number = self.take_from_storage_gui.order_phone_number.text()


        valid_address = validators.truthy(address)
        valid_date = validators.truthy(date)
        valid_phone_number = validators.length(phone_number, min=10)

        if id_order.isdigit() == True and valid_address == True and valid_date == True and valid_phone_number == True and phone_number.isdigit() == True:
            command = Commands('TakeFromStorage')
            take_from_storage = FromStorage(id_order, address, date, phone_number, TakeOrderWindow.email)

            s.sendall(pickle.dumps(command))
            time.sleep(0.00001)
            s.sendall(pickle.dumps(take_from_storage))
            bytes = s.recv(65000)
            accept = pickle.loads(bytes)
            s.close()
            if accept == True:
                back = QMessageBox()
                back.setWindowTitle('Заявка')
                back.setText("Дякуємо! Ваша заявка буде оброблена.\n З вами зв'яжеться адміністратор.")
                back.setIcon(QMessageBox.Information)
                back.setStandardButtons(QMessageBox.Ok)
                back.buttonClicked.connect(self.goback)
                back.exec_()
            else:
                back = QMessageBox()
                back.setWindowTitle('Заявка')
                back.setText("Помилка! На складі немає товару з таким номером.")
                back.setIcon(QMessageBox.Information)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()

        elif id_order.isdigit() == False:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Номер товару на складі має містити лише цифри.")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()

        elif valid_address != True:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Введіть адресу доставки.")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()

        elif valid_phone_number != True:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Номер телефону має містити 10 цифр.")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()

        elif phone_number.isdigit() == False:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Номер телефону має містити лише цифри.")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()



    def goback(self):
        self.close()
        self.userwindow = UserWindow(TakeOrderWindow.email, TakeOrderWindow.name, TakeOrderWindow.surname)
        self.userwindow.show()

    def close_w(self):
        self.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = QPoint()

    def mouseMoveEvent(self, event):
        if not self.__press_pos.isNull():
            self.move(self.pos() + (event.pos() - self.__press_pos))

    def init_UI_TakeOrderWindow(self):
        self.take_from_storage_gui.take_order_btn.clicked.connect(self.write)
        self.take_from_storage_gui.back_btn_2.clicked.connect(self.goback)
        self.take_from_storage_gui.close_btn.clicked.connect(self.close_w)
#############################################################################vlados19.2002@gmail.com

class DeleteOrderWindow(QtWidgets.QMainWindow):
    email = ''
    name = ''
    surname = ''
    def __init__(self, e_mail, name, surname):
        DeleteOrderWindow.email = e_mail
        DeleteOrderWindow.name = name
        DeleteOrderWindow.surname = surname
        super().__init__()
        self.delete_order_gui = Ui_DeleteOrderWindow()
        self.delete_order_gui.setupUi(self)
        self.__press_pos = QPoint()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.init_UI_DeleteOrderWindow()

    def delete_order(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))

        id_order = self.delete_order_gui.id_to_del_edit.text()
        valid_id_order = validators.truthy(id_order)

        if valid_id_order == True:
            command = Commands('DeleteOrder')
            info = InfoToDelOrder(id_order, DeleteOrderWindow.email)
            s.sendall(pickle.dumps(command))
            time.sleep(0.00001)
            s.sendall(pickle.dumps(info))
            bytes = s.recv(65000)
            accept = pickle.loads(bytes)
            s.close()
            if accept == True:
                back = QMessageBox()
                back.setWindowTitle('Заявка видалена')
                back.setText("Дякуємо! Ваша заявка видалена.")
                back.setIcon(QMessageBox.Information)
                back.setStandardButtons(QMessageBox.Ok)
                back.buttonClicked.connect(self.goback)
                back.exec_()
            else:
                back = QMessageBox()
                back.setWindowTitle('Помилка!')
                back.setText("У вас немає такого замовлення!.")
                back.setIcon(QMessageBox.Information)
                back.setStandardButtons(QMessageBox.Ok)
                back.exec_()
        else:
            back = QMessageBox()
            back.setWindowTitle('Помилка!')
            back.setText("Некоректно заповнені поля!")
            back.setIcon(QMessageBox.Warning)
            back.setStandardButtons(QMessageBox.Ok)
            back.exec_()


    def goback(self):
        self.close()
        self.userwindow = UserWindow(DeleteOrderWindow.email, DeleteOrderWindow.name, DeleteOrderWindow.surname)
        self.userwindow.show()

    def close_w(self):
        self.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = QPoint()

    def mouseMoveEvent(self, event):
        if not self.__press_pos.isNull():
            self.move(self.pos() + (event.pos() - self.__press_pos))

    def init_UI_DeleteOrderWindow(self):
        self.delete_order_gui.delete_btn.clicked.connect(self.delete_order)
        self.delete_order_gui.back_btn_2.clicked.connect(self.goback)
        self.delete_order_gui.close_btn.clicked.connect(self.close_w)

#############################################################################vlados19.2002@gmail.com

class AdminWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.adminisration_gui = Ui_AdminWindow()
        self.adminisration_gui.setupUi(self)
        self.__press_pos = QPoint()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.init_UI_AdminWindow()

    def scheme_of_sections(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        command = Commands('SchemeOfSections')
        s.sendall(pickle.dumps(command))
        bytes = s.recv(65000)
        section = pickle.loads(bytes)
        color = []
        for i in section:
            if 0<=i.get('COUNT(*)')<4:
                color.append("background: rgb(255, 255, 0)")
            elif 4<i.get('COUNT(*)')<8:
                color.append("background: rgb(255, 85, 0)")
            elif 8 < i.get('COUNT(*)') < 11:
                color.append("background: rgb(255, 0, 0)")
        for i in section:
            if i.get('sections') == 'A':
                self.adminisration_gui.section_A.setStyleSheet(color[0])
            if i.get('sections') == 'B':
                self.adminisration_gui.section_B.setStyleSheet(color[1])
            if i.get('sections') == 'C':
                self.adminisration_gui.section_B.setStyleSheet(color[2])

        for i in section:
            if i.get('sections') == 'A':
                self.adminisration_gui.count_A.setText('Cекція A: '+str(i.get('COUNT(*)')) +'/10')
            if i.get('sections') == 'B':
                self.adminisration_gui.count_B.setText('Cекція B: '+str(i.get('COUNT(*)')) +'/10')
            if i.get('sections') == 'C':
                self.adminisration_gui.count_C.setText('Cекція C: '+str(i.get('COUNT(*)')) +'/10')


    def all_orders(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        command = Commands('ShowAllOrders')
        s.sendall(pickle.dumps(command))
        time.sleep(0.00001)
        bytes = s.recv(65000)
        orders = pickle.loads(bytes)
        order_list = []
        for i in orders:
            order_list.append("Номер замовлення: " + str(i.get('order_id')) + '\n'+
                                   "Найменування: " + str(i.get('order_name')) +'\n' +
                                   "Дата: " + str(i.get('order_date')) + '\n'+
                                   "Номер телефону: " + str(i.get('order_phone_number'))+ '\n'+
                                   "Електронна пошта: " + str(i.get('email'))+'\n\n')
        self.close()
        self.infowindow = ShowInfoWindow(order_list, 'Заявки зберігання на складі')
        self.infowindow.show()


    def all_storage(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        command = Commands('ShowAllOrders')
        s.sendall(pickle.dumps(command))
        time.sleep(0.00001)
        bytes = s.recv(65000)
        orders = pickle.loads(bytes)
        order_list = []
        for i in orders:
            order_list.append("Cекція: " + str(i.get('sections')) + '\n'+
                                   "Номер товару: " + str(i.get('order_id')) +'\n' +
                                   "Найменування: " + str(i.get('order_name')) + '\n'+
                                   "Кількість: " + str(i.get('order_quantity'))+ '\n'+
                                    "Вага: " + str(i.get('order_weight'))+' кг' + '\n' +
                                    "Розмір: " + str(i.get('order_size')) +' м^3'+ '\n\n')
        self.close()
        self.infowindow = ShowInfoWindow(order_list, 'Cклад')
        self.infowindow.show()


    def all_clients(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, port))
        command = Commands('ShowAllClients')
        s.sendall(pickle.dumps(command))
        time.sleep(0.00001)
        bytes = s.recv(65000)
        clients = pickle.loads(bytes)
        clients_list = []
        for i in clients:
            clients_list.append("Ім'я: " + str(i.get('client_name')) + '\n'+
                                   "Прізвище: " + str(i.get('client_surname')) +'\n' +
                                   "Електронна пошта: " + str(i.get('email')) + '\n\n')
        self.close()
        self.infowindow = ShowInfoWindow(clients_list, 'Клієнти')
        self.infowindow.show()

    def goback(self):
        self.close()
        self.application = LogWindow()
        self.application.show()

    def close_w(self):
        self.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = QPoint()

    def mouseMoveEvent(self, event):
        if not self.__press_pos.isNull():
            self.move(self.pos() + (event.pos() - self.__press_pos))

    def init_UI_AdminWindow(self):
        self.scheme_of_sections()
        self.adminisration_gui.show_all_orders_btn.clicked.connect(self.all_orders)
        self.adminisration_gui.show_storage.clicked.connect(self.all_storage)
        self.adminisration_gui.show_all_clients_btn.clicked.connect(self.all_clients)
        self.adminisration_gui.back_btn_2.clicked.connect(self.goback)
        self.adminisration_gui.close_btn.clicked.connect(self.close_w)

class ShowInfoWindow(QtWidgets.QMainWindow):
    info = ''
    title = ''
    def __init__(self, info, title):
        super().__init__()
        ShowInfoWindow.info = info
        ShowInfoWindow.title = title
        self.show_info_gui = Ui_ShowInfoWindow()
        self.show_info_gui.setupUi(self)
        #self.show_info_gui.info_line_edit.setText(ShowInfoWindow.show_all_info)
        self.__press_pos = QPoint()
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        self.init_UI_ShowInfoWindow()

    def goback(self):
        self.close()
        self.adminwindow = AdminWindow()
        self.adminwindow.show()

    def close_w(self):
        self.close()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = event.pos()

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.__press_pos = QPoint()

    def mouseMoveEvent(self, event):
        if not self.__press_pos.isNull():
            self.move(self.pos() + (event.pos() - self.__press_pos))

    def set_info(self):
        self.show_info_gui.info_text.setFont(QtGui.QFont("Times", 12 ))
        self.show_info_gui.info_text.setText(ShowInfoWindow.title +'\n')
        self.show_info_gui.info_text.setAlignment(Qt.AlignCenter)
        self.show_info_gui.info_text.append(str(''.join(ShowInfoWindow.info)))
        self.show_info_gui.info_text.setAlignment(Qt.AlignCenter)

    def init_UI_ShowInfoWindow(self):
        self.set_info()
        self.show_info_gui.back_btn_2.clicked.connect(self.goback)
        self.show_info_gui.close_btn.clicked.connect(self.close_w)




app = QtWidgets.QApplication([])
application = LogWindow()
application.show()
sys.exit(app.exec_())

