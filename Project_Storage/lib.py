class ClientRegistration:
    def __init__(self, name,surname, email, password, status):
        self.name  = name
        self.surname  = surname
        self.email = email
        self.password = password
        self.status = status

class CheckEmail:
    def __init__(self, email):
        self.email = email


class ClientLogIn:
    def __init__(self, email, password):
        self.email = email
        self.password = password

class Commands:
    def __init__(self, instraction):
        self.instraction = instraction

class Email:
    def __init__(self, email):
        self.email = email

class InfoToDelOrder:
    def __init__(self,id_order, email):
        self.id_order = id_order
        self.email = email


class ToStorage:
    def __init__(self, name, quantity, weight, size, date, phone_number, email):
        self.name = name
        self.quantity = quantity
        self.weight = weight
        self.size = size
        self.date = date
        self.phone_number = phone_number
        self.email = email

class FromStorage:
    def __init__(self, id_order, address, date, phone_number, email):
        self.id_order = id_order
        self.address = address
        self.date = date
        self.phone_number = phone_number
        self.email = email