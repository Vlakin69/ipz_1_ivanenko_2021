# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'sign_in_gui.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_LoginWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(600, 556)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(600, 556))
        MainWindow.setMaximumSize(QtCore.QSize(600, 556))
        font = QtGui.QFont()
        font.setPointSize(10)
        MainWindow.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet("background: #444444")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(150, 60, 300, 401))
        self.frame.setStyleSheet("background: #EDEDED;\n"
"border-radius: 15px;")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.user_email = QtWidgets.QLineEdit(self.frame)
        self.user_email.setGeometry(QtCore.QRect(70, 110, 160, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.user_email.setFont(font)
        self.user_email.setStyleSheet("background: rgb(186, 185, 185);\n"
"border-radius: 10px;")
        self.user_email.setObjectName("user_email")
        self.user_password = QtWidgets.QLineEdit(self.frame)
        self.user_password.setGeometry(QtCore.QRect(70, 170, 160, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.user_password.setFont(font)
        self.user_password.setStyleSheet("background: rgb(186, 185, 185);\n"
"border-radius: 10px;")
        self.user_password.setObjectName("user_password")
        self.label = QtWidgets.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(0, 0, 300, 78))
        self.label.setStyleSheet("font: 16pt \"MS Shell Dlg 2\";\n"
"background:#DA0037;\n"
"color: white;")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.login_btn = QtWidgets.QPushButton(self.frame)
        self.login_btn.setGeometry(QtCore.QRect(70, 230, 160, 30))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        self.login_btn.setFont(font)
        self.login_btn.setStyleSheet("background:#DA0037;\n"
"color: white;\n"
"font: 10pt \"MS Shell Dlg 2\";")
        self.login_btn.setObjectName("login_btn")
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(60, 330, 171, 20))
        self.label_2.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";\n"
"color:rgb(144, 144, 144);")
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")
        self.to_reg_btn = QtWidgets.QPushButton(self.frame)
        self.to_reg_btn.setGeometry(QtCore.QRect(60, 350, 171, 30))
        self.to_reg_btn.setStyleSheet("font: 10pt \"MS Shell Dlg 2\";\n"
"color: #DA0037;\n"
"")
        self.to_reg_btn.setObjectName("to_reg_btn")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Storage"))
        self.user_email.setPlaceholderText(_translate("MainWindow", "Емейл"))
        self.user_password.setPlaceholderText(_translate("MainWindow", "Пароль"))
        self.label.setText(_translate("MainWindow", "ВХІД"))
        self.login_btn.setText(_translate("MainWindow", "Увійти"))
        self.label_2.setText(_translate("MainWindow", "Не маєте облікового запису?"))
        self.to_reg_btn.setText(_translate("MainWindow", "ЗАРЕЄСТРУВАТИСЯ "))
